#!/usr/bin/python

from lxml import etree
import re
class snip:
  def __init__(self,name,ftype=''):
    self.root=etree.Element("snippets", namespace="", license="GPL v3+", filetypes="Fortran", authors="",name=name)
    etree.SubElement(self.root,"script")
    print 'initialized snip element'
  def fixContent(self,vimsnipcontent):
    cursorOn=False
    varlist=[]
    newstring=''
    
    for l in vimsnipcontent.split('\n'):
      if re.search('\$\{[0-9]+\:[a-z0-9A-Z!* ]+\}',l):
        for el in re.findall('\$\{[0-9]+\:[a-z0-9A-Z!* ]+\}',l):
          varlist.append([re.sub('\:[a-z0-9A-Z! ]+\}','',re.sub('\$\{','',el)),re.sub('\$\{[0-9]+\:','',el)])
    for l in vimsnipcontent.split('\n'):
      if re.search('\$\{[0-9]+\:[a-z0-9A-Z!* ]+\}',l):
        newstring+=re.sub('\$\{[0-9]+\:', '${', l)+'\n'
      elif re.search('\$\{*[0-9]+\}*',l):
        found=False
        nu=re.sub('\}','',re.sub('\$\{*','',l))
        for el in varlist:
          if re.search('\$\{*'+el[0]+'\}*',l):
            found=True
          l=re.sub('\$\{*'+el[0]+'\}*','${'+el[1]+'}',l)
        if not found:
          #assuming this is just cursor position..
          newstring+=re.sub('\$\{*[0-9]+\}*', '${cursor}', l)+'\n'
      else:
        newstring+=re.sub('\$\{[0-9]+\:','${',l)+'\n'
    
    #print varlist
    print newstring
    return newstring
  def addSnippet(self,snipname,snipcontent):
    newsnip=etree.SubElement(self.root,"item")
    
    
    etree.SubElement(newsnip,"displayprefix")
    etree.SubElement(newsnip,"match")
    etree.SubElement(newsnip,"displaypostfix")
    etree.SubElement(newsnip,"displayarguments")
    etree.SubElement(newsnip,"fillin")
    
    newsnip.find("match").text=snipname
    
    newsnip.find("fillin").text=self.fixContent(snipcontent)
    print "added snippet:",newsnip[1].text
  def setLicense(license):
    self.root.set("license",license)
  def getall(self):
    return etree.tostring(self.root, pretty_print=True)






def main(filein,fileout):
  """read in file and convert to ktextmate xml format"""
  
  f1=file(filein,'r')
  f2=file(fileout,'w')
  
  s=snip("vimsnippets-Fortran") 

  while True:
    l=f1.readline()
    if len(l)==0:
      break #end of file reached
    if l[0:7]=='snippet':
      snipname=l.split()[1]
      l=f1.readline() # I now require at least one comment line between each snippet.. not a good idea!
      snipcontent=''
      while l[0]=='\t':
        snipcontent+=l[1:]
        l=f1.readline()
      s.addSnippet(snipname,snipcontent)
  f2.write(s.getall())
  f2.close()

if __name__ == '__main__':
  main("snippets/fortran.snippets","ktextmate/Fortran Snippets.xml")
  main("snippets/python.snippets","ktextmate/Python Snippets.xml")
